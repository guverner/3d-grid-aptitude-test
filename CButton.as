﻿package
{
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;

	public class CButton extends Sprite
	{
		private var m_width:Number;
		private var m_height:Number;
		private var m_label:TextField;
		private var m_text:String;
		private var m_callback:Function;
		private var m_isMouseOver:Boolean;
		
		public function CButton(text_:String, width_:Number, height_:Number, callback_:Function)
		{
			m_text = text_;
			m_width = width_;
			m_height = height_;
			m_callback = callback_;
			m_isMouseOver = false;
			
			if(this.stage)
			{
				initialize();
			}
			else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
		}
		
		private function onAddedToStage(e:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialize();
		}
		
		private function initialize():void
		{
			this.buttonMode = true;
			
			this.graphics.lineStyle(1, 0x000000);
			this.graphics.beginFill(0xffffff);
			this.graphics.drawRect(-m_width / 2, -m_height / 2, m_width, m_height);
			this.graphics.endFill();
			
			this.graphics.beginFill(0xffffff);
			this.graphics.drawRect((-m_width) / 2 + 3, (-m_height) / 2 + 3, m_width - 6, m_height - 6);
			this.graphics.endFill();

			m_label = initLabel(m_text, 12);
			m_label.x -= m_label.width / 2;
			m_label.y -= m_label.height / 2 + 5;
			this.addChild(m_label);
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, onButtonDown);
			this.addEventListener(MouseEvent.MOUSE_UP, onButtonUp);
			this.addEventListener(MouseEvent.MOUSE_OVER, onButtonOver);
			this.addEventListener(MouseEvent.MOUSE_OUT, onButtonOut);
		}
		
		private function onButtonDown(e:MouseEvent):void
		{
			this.scaleX = this.scaleY = 0.9;
		}
		
		private function onButtonUp(e:MouseEvent):void
		{
			if(m_isMouseOver)
			{
				this.scaleX = this.scaleY = 1.2;
			}
			else
			{
				this.scaleX = this.scaleY = 1;
			}
			
			if(m_isMouseOver)
			{
				m_callback();
			}
		}
		
		private function onButtonOver(e:MouseEvent):void
		{
			m_isMouseOver = true;
			this.scaleX = this.scaleY = 1.2;
		}
		
		private function onButtonOut(e:MouseEvent):void
		{
			m_isMouseOver = false;
			this.scaleX = this.scaleY = 1;
		}
		
		private function initLabel(text_:String, fontSize_:Number):TextField
		{
			var textFormat:TextFormat = new TextFormat();
			textFormat.size = fontSize_;
			textFormat.align = TextFormatAlign.CENTER;
			
			var label:TextField = new TextField();
			label.mouseEnabled = false;
			label.defaultTextFormat = textFormat;
			label.width = this.width;
			label.height = 20;
			label.text  = text_;
			label.y = this.height / 2 - label.height / 2;
			
			return label;
		}
	}
}
