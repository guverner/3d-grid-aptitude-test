﻿package
{
	import flash.display.Sprite;

	public class Tile extends Sprite
	{
		public static const SIDE_BACK:int = 	0;
		public static const SIDE_FRONT:int = 	1;
		public static const SIDE_RIGHT:int = 	2;
		public static const SIDE_LEFT:int = 	3;
		public static const SIDE_DOWN:int = 	4;
		public static const SIDE_UP:int = 		5;
		private var m_sides:Array;
		private var m_row:int;
		private var m_column:int;
		private var m_height:int;
		private var m_size:Number;
		
		public function Tile(size_:Number, row_:int, column_:int, height_:int = 0)
		{
			m_size = size_;
			m_row = row_;
			m_column = column_;
			m_height = height_;
			
			m_sides = new Array(6);
			
			var indSide:int;
			
			for(indSide = 0; indSide < m_sides.length; indSide++)
			{
				var side:Sprite = createSide(indSide, m_size, m_height);
				m_sides[indSide] = side;
			}

			this.addChild(m_sides[SIDE_BACK]);
			this.addChild(m_sides[SIDE_LEFT]);
			//this.addChild(m_sides[SIDE_DOWN]);
			this.addChild(m_sides[SIDE_RIGHT]);
			this.addChild(m_sides[SIDE_FRONT]);
			this.addChild(m_sides[SIDE_UP]);

			m_sides[SIDE_BACK].z = size_ / 2;
			m_sides[SIDE_FRONT].z = -size_ / 2;
			m_sides[SIDE_RIGHT].x = size_ / 2;
			m_sides[SIDE_LEFT].x = -size_ / 2;
			m_sides[SIDE_UP].y = size_ / 2 - height_;
			if(height_ < 0)
			{
				m_sides[SIDE_DOWN].y = size_ / 2 - height_;
			}
			else
			{
				m_sides[SIDE_DOWN].y = size_ / 2;
			}
			
			if(height_ <= 0)
			{
				m_sides[SIDE_BACK].y = size_ / 2 - height_ / 2 - height_ / 2;
				m_sides[SIDE_FRONT].y = size_ / 2 - height_ / 2 - height_ / 2;
				m_sides[SIDE_RIGHT].y = size_ / 2 - height_ / 2 - height_ / 2;
				m_sides[SIDE_LEFT].y = size_ / 2 - height_ / 2 - height_ / 2;
			}
			else
			{
				m_sides[SIDE_BACK].y = size_ / 2 - height_ / 2;
				m_sides[SIDE_FRONT].y = size_ / 2 - height_ / 2;
				m_sides[SIDE_RIGHT].y = size_ / 2 - height_ / 2;
				m_sides[SIDE_LEFT].y = size_ / 2 - height_ / 2;
			}
			
			m_sides[SIDE_RIGHT].rotationY = 90;
			m_sides[SIDE_LEFT].rotationY = 90;
			m_sides[SIDE_DOWN].rotationX = 90;
			m_sides[SIDE_UP].rotationX = 90;
		}
		
		public function drawSide(side:Sprite, size_:Number, height_:int, sideType:int, extraHeight_:int = 0):void
		{
			var color:uint = findColorForSide(sideType);
			
			side.graphics.clear();
			
			side.graphics.lineStyle(1, 0x000000);
			
			side.graphics.beginFill(color);
			if(sideType < 4)//if its vertical sides
			{
				if(height_ <= 0)
				{
					side.graphics.drawRect(-size_ / 2, 0, size_, Math.abs(extraHeight_));
				}
				else
				{
					side.graphics.drawRect(-size_ / 2, -height_ / 2, size_, height_ + extraHeight_);
				}
			}
			else//it its horizontal sides (up and down)
			{
				side.graphics.drawRect(-size_ / 2, -size_ / 2, size_, size_);
			}
			side.graphics.endFill();
		}
		
		private function createSide(sideType:int, size_:Number, height_:int):Sprite
		{
			var side:Sprite = new Sprite();
			
			drawSide(side, size_, height_, sideType);
			
			return side;
		}
		
		public function get size():Number
		{
			return m_size;
		}
		
		public function get tileHeight():int
		{
			return m_height;
		}
		
		public function get row():int
		{
			return m_row;
		}
		
		public function get column():int
		{
			return m_column;
		}
		
		public function get sides():Array
		{
			return m_sides;
		}
		
		public function getSide(sideType:int):Sprite
		{
			if(sideType < 0 || sideType >= m_sides.length)
			{
				return null;
			}
			else
			{
				return m_sides[sideType];
			}
		}
		
		public function updateByHeight(height_:int):void
		{
			m_height += (height_ * -1);
			
			if(m_height > 150)
			{
				m_height = 150;
			}
			else if(m_height < -150)
			{
				m_height = -150;
			}
			
			var upSide:Sprite = this.getSide(Tile.SIDE_UP);
			upSide.y = m_size / 2 - m_height;
			
			drawSide(m_sides[SIDE_RIGHT], m_size, m_height, SIDE_RIGHT);
			drawSide(m_sides[SIDE_FRONT], m_size, m_height, SIDE_FRONT);
			drawSide(m_sides[SIDE_LEFT], m_size, m_height, SIDE_LEFT);
			drawSide(m_sides[SIDE_BACK], m_size, m_height, SIDE_BACK);
			
			if(m_height <= 0)
			{
				m_sides[SIDE_BACK].y = m_size / 2 - m_height;
				m_sides[SIDE_FRONT].y = m_size / 2 - m_height;
				m_sides[SIDE_RIGHT].y = m_size / 2 - m_height;
				m_sides[SIDE_LEFT].y = m_size / 2 - m_height;
			}
			else
			{
				m_sides[SIDE_BACK].y = m_size / 2 - m_height / 2;
				m_sides[SIDE_FRONT].y = m_size / 2 - m_height / 2;
				m_sides[SIDE_RIGHT].y = m_size / 2 - m_height / 2;
				m_sides[SIDE_LEFT].y = m_size / 2 - m_height / 2;
			}
			
			updateSidesAccordingNearTiles();
		}
		
		private function updateSidesAccordingNearTiles()
		{
			var tiles:Array = Main.instance.currentTiles;
			
			changeSide(SIDE_RIGHT, tiles);
			changeSide(SIDE_FRONT, tiles);
			changeSide(SIDE_LEFT, tiles);
			changeSide(SIDE_BACK, tiles);
		}
		
		private function changeSide(sideType:int, tiles:Array):void
		{
			if(!tiles || tiles.length == 0)
			{
				return;
			}
			
			var side:Sprite = m_sides[sideType];
			var nearTile:Tile = null;
			var nearTileHeight:int;
			var nearTileSize:Number;
			
			if(sideType == SIDE_RIGHT && Main.isIndicesWithin2DArray(tiles, m_row + 1, m_column))
			{
				nearTile = tiles[m_row + 1][m_column] as Tile;
			}
			else if(sideType == SIDE_FRONT && Main.isIndicesWithin2DArray(tiles, m_row, m_column + 1))
			{
				nearTile = tiles[m_row][m_column + 1] as Tile;
			}
			else if(sideType == SIDE_LEFT && Main.isIndicesWithin2DArray(tiles, m_row - 1, m_column))
			{
				nearTile = tiles[m_row - 1][m_column] as Tile;
			}
			else if(sideType == SIDE_BACK && Main.isIndicesWithin2DArray(tiles, m_row, m_column - 1))
			{
				nearTile = tiles[m_row][m_column - 1] as Tile;
			}
			else
			{
				return;
			}
			
			nearTileHeight = nearTile.tileHeight;
			nearTileSize = nearTile.size;
			
			if(nearTileHeight < m_height && nearTileHeight < 0)
			{
				if(m_height >= 0)
				{
					drawSide(side, m_size, m_height, sideType, -nearTileHeight);
				}
				else
				{
					drawSide(side, m_size, m_height, sideType, m_height + -nearTileHeight);
				}
			}
		}
		
		public function findColorForSide(indSide:int):uint
		{
			if(indSide == 5)
			{
				return 0xe1e1e1;
			}
			else if(indSide == 2 || indSide == 3)
			{
				return 0x565656;
			}
			else
			{
				return 0x9a9a9a;
			}
		}
	}
}
