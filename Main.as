﻿package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.display.MovieClip;
	import flash.geom.Vector3D;
	import flash.events.MouseEvent;
	import flash.display.CapsStyle;
	import flash.display.JointStyle;
	import flash.geom.Point;
	import flash.display.Shape;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.net.FileReference;
	
	[SWF(width="800", height="600", backgroundColor="#ffffff", frameRate="30")]
	public class Main extends Sprite
	{
		private static const NUM_ROWS:uint = 	10;
		private static const NUM_COLUMNS:uint = 10;
		private static const CELL_SIZE:Number = 29;
		private var m_tiles:Array;
		private var m_isMouseDown:Boolean;
		private var m_currentTile:Tile;
		private var m_heightmaps:Array;
		private var m_heightmapExtra:Sprite;
		private var m_tilesExtra:Array;
		private var m_ruler:Sprite;
		private var m_currentHeightmapIndex:int;
		private const NUM_HEIGHTMAPS:uint = 3;
		private var m_fileReference:FileReference;
		
		private static const POS_FRONT:int = 	1;
		private static const POS_BACK:int = 	2;
		private static const POS_LEFT:int = 	3;
		private static const POS_RIGHT:int = 	4;
		
		private static const DIR_NONE:int = 	0;
		private static const DIR_LEFT:int = 	1;
		private static const DIR_RIGHT:int = 	2;
		private static const DIR_UP:int = 		3;
		private static const DIR_DOWN:int = 	4;
		private var m_prevMouseX:Number;
		private var m_prevMouseY:Number;
		private var m_currentMouseX:Number;
		private var m_currentMouseY:Number;
		private var m_mouseXDir:int;
		private var m_mouseYDir:int;
		private var m_heightmapXMLs:Array;
		private var m_heightmapXMLExtra:XML;
		
		private var m_importHeightMapButton:CButton;
		private var m_exportHeightMapButton:CButton;
		private var m_heightMap1Button:CButton;
		private var m_heightMap2Button:CButton;
		private var m_heightMap3Button:CButton;
		
		private static var m_instance:Main;
		
		public function Main()
		{
			m_instance = this;
			
			if(this.stage)
			{
				initialize();
			}
			else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
		}
		
		private function onAddedToStage(e:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialize();
		}
		
		private function initialize():void
		{
			m_fileReference = new FileReference();
			
			m_heightmapXMLs = new Array(NUM_HEIGHTMAPS);
			
			loadheightmapXMLs();
			
			m_tiles = new Array(NUM_HEIGHTMAPS);
			m_isMouseDown = false;
			m_currentTile = null;
			m_heightmaps = new Array(NUM_HEIGHTMAPS);
			
			m_heightmapExtra = new Sprite();
			m_heightmapExtra.z = 1;
			m_heightmapExtra.y = -30;
			m_heightmapExtra.transform.matrix3D.prependRotation(45, Vector3D.X_AXIS);
			m_heightmapExtra.transform.matrix3D.prependRotation(45, Vector3D.Y_AXIS);
			m_heightmapExtra.visible = false;
			this.addChild(m_heightmapExtra);
			m_tilesExtra = new Array();
			
			for(var indHeightmap:int = 0; indHeightmap < NUM_HEIGHTMAPS; indHeightmap++)
			{
				var heighmap:Sprite = new Sprite();
				heighmap.z = 1;
				heighmap.y = -30;
				heighmap.transform.matrix3D.prependRotation(45, Vector3D.X_AXIS);
				heighmap.transform.matrix3D.prependRotation(45, Vector3D.Y_AXIS);
				heighmap.visible = false;
				this.addChild(heighmap);
				m_heightmaps[indHeightmap] = heighmap;
				
				var tilesStorage:Array = new Array();
				m_tiles[indHeightmap] = tilesStorage;
			}
			
			this.transform.perspectiveProjection.fieldOfView = 45;
			this.transform.perspectiveProjection.focalLength = 50000;
			
			creatInterface();
			
			stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownEvent);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUpEvent);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveEvent);
		}
		
		public static function get instance():Main
		{
			return m_instance;
		}
		
		private function loadheightmapXMLs():void
		{
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, readXML);
			loader.load(new URLRequest("heightmap1.xml"));
			
			loader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, readXML);
			loader.load(new URLRequest("heightmap2.xml"));
			
			loader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, readXML);
			loader.load(new URLRequest("heightmap3.xml"));
		}
		
		private function readXML(e:Event):void
		{
			var xml:XML = new XML(e.target.data);
			var heightmapName:String = xml.@name;
			
			switch(heightmapName)
			{
				case "heightmap1":
					m_heightmapXMLs[0] = xml;
					break;
				case "heightmap2":
					m_heightmapXMLs[1] = xml;
					break;
				case "heightmap3":
					m_heightmapXMLs[2] = xml;
					break;
			}
			
			if(m_heightmapXMLs[0] && m_heightmapXMLs[1] && m_heightmapXMLs[2])
			{
				onXMLReaded();
			}
		}
		
		private function onXMLReaded():void
		{
			makeHeightmapVisible(0);
			m_currentHeightmapIndex = 0;
			loadMap(m_heightmapXMLs[0]);
			m_currentHeightmapIndex = 1;
			loadMap(m_heightmapXMLs[1]);
			m_currentHeightmapIndex = 2;
			loadMap(m_heightmapXMLs[2]);
			m_currentHeightmapIndex = 0;
		}
		
		public function get tiles():Array
		{
			return m_tiles;
		}
		
		public function get currentTiles():Array
		{
			if(m_currentHeightmapIndex == 3)
			{
				return m_tilesExtra;
			}
			else
			{
				return m_tiles[m_currentHeightmapIndex];
			}
		}
		
		private function creatInterface():void
		{
			m_ruler = initRuler();
			m_ruler.x = 164;
			m_ruler.y = 258;
			m_ruler.z = 1;
			m_ruler.transform.matrix3D.prependRotation(45, Vector3D.X_AXIS);
			m_ruler.transform.matrix3D.prependRotation(45, Vector3D.Y_AXIS);
			this.addChild(m_ruler);
			
			m_importHeightMapButton = new CButton("Импорт набора высот", 140, 28, onImportHeightMapButtonDown);
			m_importHeightMapButton.x = 650;
			m_importHeightMapButton.y = 530;
			this.addChild(m_importHeightMapButton);
			
			m_exportHeightMapButton = new CButton("Экспорт набора высот", 140, 28, onExportHeightMapButtonDown);
			m_exportHeightMapButton.x = 650;
			m_exportHeightMapButton.y = 575;
			this.addChild(m_exportHeightMapButton);
			
			m_heightMap1Button = new CButton("Набор высот № 1", 140, 28, onLoadHeightmap1ButtonDown);
			m_heightMap1Button.x = 130;
			m_heightMap1Button.y = 530;
			this.addChild(m_heightMap1Button);
			
			m_heightMap2Button = new CButton("Набор высот № 2", 140, 28, onLoadHeightmap2ButtonDown);
			m_heightMap2Button.x = 290;
			m_heightMap2Button.y = 530;
			this.addChild(m_heightMap2Button);
			
			m_heightMap3Button = new CButton("Набор высот № 3", 140, 28, onLoadHeightmap3ButtonDown);
			m_heightMap3Button.x = 450;
			m_heightMap3Button.y = 530;
			this.addChild(m_heightMap3Button);
		}
		
		private function onImportHeightMapButtonDown():void
		{
			m_fileReference.addEventListener(Event.SELECT, onSelectXML);
    		m_fileReference.browse();
		}
		
		private function onSelectXML(e:Event):void
		{
			m_fileReference.addEventListener(Event.COMPLETE, onLoadXML);
    		m_fileReference.load();
		}
		
		private function onLoadXML(e:Event):void
		{
			var xml:XML = new XML(e.target.data);
			m_heightmapXMLExtra = xml;
			
			m_currentHeightmapIndex = 3;
			removeAllTilesFrom(m_currentHeightmapIndex);
			loadMap(xml);
			makeHeightmapVisible(m_currentHeightmapIndex);
		}
		
		private function onExportHeightMapButtonDown():void
		{
			saveHeightmap(m_currentHeightmapIndex);
		}
		
		private function saveHeightmap(heightmapIndex:int):void
		{
			var heightmapXML:XML = null;
			var tilesArray:Array = null;
			
			if(heightmapIndex == 3)
			{
				heightmapXML = m_heightmapXMLExtra;
				tilesArray = m_tilesExtra
			}
			else if(heightmapIndex >= 0 && heightmapIndex < NUM_HEIGHTMAPS)
			{
				heightmapXML = m_heightmapXMLs[heightmapIndex];
				tilesArray = m_tiles[m_currentHeightmapIndex];
			}
			
			var indRow:int;
			var indColumn:int;
			var i:int = 0;
			
			for(indRow = 0; indRow < NUM_ROWS; indRow++)
			{
				for(indColumn = 0; indColumn < NUM_COLUMNS; indColumn++)
				{
					var tileXML:XML = heightmapXML.TILES.TILE[i];
					tileXML.@height = (tilesArray[indRow][indColumn] as Tile).tileHeight;
					i++;
				}
			}
			
			m_fileReference = new FileReference();
			m_fileReference.save(heightmapXML, ".xml");//, heightmapXML.@name + ".xml"
		}
		
		private function onLoadHeightmap1ButtonDown():void
		{
			m_currentHeightmapIndex = 0;
			makeHeightmapVisible(m_currentHeightmapIndex);
		}
		
		private function onLoadHeightmap2ButtonDown():void
		{
			m_currentHeightmapIndex = 1;
			makeHeightmapVisible(m_currentHeightmapIndex);
		}
		
		private function onLoadHeightmap3ButtonDown():void
		{
			m_currentHeightmapIndex = 2;
			makeHeightmapVisible(m_currentHeightmapIndex);
		}
		
		private function removeAllTilesFrom(indHeightmap:int):void
		{
			var heightmap:Sprite = new Sprite();
			this.addChild(heightmap);
			
			if(indHeightmap == 3)
			{
				this.removeChild(m_heightmapExtra);
				m_heightmapExtra = heightmap;
				
				m_tilesExtra = new Array();
			}
			else
			{
				this.removeChild(m_heightmaps[indHeightmap]);
				m_heightmaps[indHeightmap] = heightmap;
				
				m_tiles[indHeightmap] = new Array();
			}
			
			heightmap.z = 1;
			heightmap.y = -30;
			heightmap.transform.matrix3D.prependRotation(45, Vector3D.X_AXIS);
			heightmap.transform.matrix3D.prependRotation(45, Vector3D.Y_AXIS);
		}
		
		private function createTile(indRow:int, indColumn:int, height_:int):void
		{
			var tile:Tile = new Tile(CELL_SIZE, indRow, indColumn, height_);
			tile.x = 350 + indRow * CELL_SIZE;
			tile.z = 180 - indColumn * CELL_SIZE;
			tile.y = 90;
			
			if(m_currentHeightmapIndex == 3)
			{
				m_heightmapExtra.addChild(tile);
				m_tilesExtra[indRow].push(tile);
			}
			else
			{
				m_heightmaps[m_currentHeightmapIndex].addChild(tile);
				m_tiles[m_currentHeightmapIndex][indRow].push(tile);
			}
			
			checkNearBordersForTile(tile, tile.tileHeight);
		}
		
		private function makeHeightmapVisible(heightmapIndex:int):void
		{
			if(heightmapIndex == 3)
			{
				m_heightmapExtra.visible = true;
			}
			else
			{
				(m_heightmaps[heightmapIndex] as Sprite).visible = true;
			}
			
			switch(heightmapIndex)
			{
				case 0:
					(m_heightmaps[1] as Sprite).visible = false;
					(m_heightmaps[2] as Sprite).visible = false;
					m_heightmapExtra.visible = false;
					break;
				case 1:
					(m_heightmaps[0] as Sprite).visible = false;
					(m_heightmaps[2] as Sprite).visible = false;
					m_heightmapExtra.visible = false;
					break;
				case 2:
					(m_heightmaps[0] as Sprite).visible = false;
					(m_heightmaps[1] as Sprite).visible = false;
					m_heightmapExtra.visible = false;
					break;
				case 3:
					(m_heightmaps[0] as Sprite).visible = false;
					(m_heightmaps[1] as Sprite).visible = false;
					(m_heightmaps[2] as Sprite).visible = false;
					break;
			}
		}
		
		private function loadMap(heightmapXML:XML):void
		{
			if(heightmapXML)
			{
				var indRow:int = 0;
				var indColumn:int = 0;
				
				var o:XML;
				for each(o in heightmapXML.TILES[0].TILE)
				{
					var height_:int = o.@height;
					
					if(indColumn > NUM_COLUMNS - 1)
					{
						indColumn = 0;
						indRow++;
					}
					
					if(indColumn == 0)
					{
						if(m_currentHeightmapIndex == 3)
						{
							m_tilesExtra.push(new Array());
						}
						else
						{
							m_tiles[m_currentHeightmapIndex].push(new Array());
						}
					}
					
					createTile(indRow, indColumn, height_);
					
					indColumn++;
				}
			}
		}
		
		private function changeNearTileAccordingToTile(nearTile:Tile, tile:Tile, prevTileHeight:int, posNearTile:int):void
		{
			var tileHeight:int = tile.tileHeight;
			var nearTileHeight:int = nearTile.tileHeight;
			var nearTileSize:Number = nearTile.size;
			var color:uint;
			
			var side:Sprite = null;
			
			if(posNearTile == POS_LEFT)
			{
				side = nearTile.getSide(Tile.SIDE_RIGHT);
				color = nearTile.findColorForSide(Tile.SIDE_RIGHT);
			}
			else if(posNearTile == POS_BACK)
			{
				side = nearTile.getSide(Tile.SIDE_FRONT);
				color = nearTile.findColorForSide(Tile.SIDE_FRONT);
			}
			else if(posNearTile == POS_RIGHT)
			{
				side = nearTile.getSide(Tile.SIDE_LEFT);
				color = nearTile.findColorForSide(Tile.SIDE_LEFT);
			}
			else if(posNearTile == POS_FRONT)
			{
				side = nearTile.getSide(Tile.SIDE_BACK);
				color = nearTile.findColorForSide(Tile.SIDE_BACK);
			}
			else
			{
				return;
			}
			
			if(prevTileHeight < 0 && tileHeight > 0)
			{
				side.graphics.clear();
				side.graphics.lineStyle(1, 0x000000);
				side.graphics.beginFill(color);
				
				if(nearTileHeight < 0)
				{
					side.y = nearTileSize / 2 - nearTileHeight;
					side.graphics.drawRect(-nearTileSize / 2, 0, nearTileSize, 0);
				}
				else
				{
					side.graphics.drawRect(-nearTileSize / 2, -nearTileHeight / 2, nearTileSize, nearTileHeight);
				}
				
				side.graphics.endFill();
			}
			else if(tileHeight < 0 && nearTileHeight > tileHeight)
			{//
				side.y = nearTileSize / 2 - nearTileHeight / 2;
				
				side.graphics.clear();
				side.graphics.lineStyle(1, 0x000000);
				side.graphics.beginFill(color);
				side.graphics.drawRect(-nearTileSize / 2, -nearTileHeight / 2, nearTileSize, nearTileHeight + Math.abs(tileHeight));
				side.graphics.endFill();
			}
			else if(tileHeight < 0 && nearTileHeight < 0 && tileHeight > nearTileHeight)
			{
				side.y = nearTileSize / 2 - nearTileHeight / 2;
				
				side.graphics.clear();
				side.graphics.lineStyle(1, 0x000000);
				side.graphics.beginFill(color);
				side.graphics.drawRect(-nearTileSize / 2, -nearTileHeight / 2, nearTileSize, 0);
				side.graphics.endFill();
			}
		}
		
		private function checkNearBordersForTile(tile:Tile, prevTileHeight:int):void
		{
			if(!tile)
			{
				return;
			}
			
			var tilesArray:Array = null;
			
			if(m_currentHeightmapIndex == 3)
			{
				tilesArray = m_tilesExtra as Array;
			}
			else
			{
				tilesArray = m_tiles[m_currentHeightmapIndex] as Array;
			}
			
			if(isIndicesWithin2DArray(tilesArray, tile.row - 1, tile.column))
			{
				changeNearTileAccordingToTile(tilesArray[tile.row - 1][tile.column] as Tile, tile, prevTileHeight, POS_LEFT);
			}
			
			if(isIndicesWithin2DArray(tilesArray, tile.row, tile.column - 1))
			{
				changeNearTileAccordingToTile(tilesArray[tile.row][tile.column - 1] as Tile, tile, prevTileHeight, POS_BACK);
			}
			
			if(isIndicesWithin2DArray(tilesArray, tile.row + 1, tile.column))
			{
				changeNearTileAccordingToTile(tilesArray[tile.row + 1][tile.column] as Tile, tile, prevTileHeight, POS_RIGHT);
			}
			
			if(isIndicesWithin2DArray(tilesArray, tile.row, tile.column + 1))
			{
				changeNearTileAccordingToTile(tilesArray[tile.row][tile.column + 1] as Tile, tile, prevTileHeight, POS_FRONT);
			}
		}
		
		public static function isIndicesWithin2DArray(array:Array, row:int, column:int):Boolean
		{
			if(array && array.length != 0)
			{
				if(row >= 0 && row < array.length)
				{
					var rowArray:Array = array[row] as Array;
					
					if(rowArray && rowArray.length > 0 && column >= 0 && column < rowArray.length)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		
		private function initRuler():Sprite
		{
			var ruler:Sprite = new Sprite();
			
			var shapeRuler:Shape = new Shape();
			ruler.addChild(shapeRuler);
			
			shapeRuler.graphics.lineStyle(1, 0x000000);
			shapeRuler.graphics.moveTo(0, -180);
			shapeRuler.graphics.lineTo(0, 180);
			
			var labelScale:TextField = null;
			var myFormat:TextFormat = null;
			
			var i:int = 0;
			for(i = 0; i < 7; i++)
			{
				shapeRuler.graphics.moveTo(0, -150 + 50 * i);
				shapeRuler.graphics.lineTo(-35, -150 + 50 * i);
				
				myFormat = new TextFormat();
				myFormat.size = 15;
				myFormat.align = TextFormatAlign.RIGHT;
				
				labelScale = new TextField();
				labelScale.defaultTextFormat = myFormat;
				//labelScale.border = true;
				labelScale.width = 30;
				labelScale.height = 20;
				labelScale.text = (150 - 50 * i).toString();
				labelScale.x = -33;
				labelScale.y = -170 + 50 * i;
				ruler.addChild(labelScale);
			}
import flash.display.Sprite;
			
			//shapeRuler.graphics.moveTo(0, -150);
//			shapeRuler.graphics.lineTo(-30, -150);
//			shapeRuler.graphics.moveTo(0, -100);
//			shapeRuler.graphics.lineTo(-30, -100);
			
			return ruler;
		}
		
		private function onMouseMoveEvent(e:MouseEvent):void
		{
			m_prevMouseX = m_currentMouseX;
			m_prevMouseY = m_currentMouseY;
			m_currentMouseX = this.stage.mouseX;
			m_currentMouseY = this.stage.mouseY;
			
    		m_mouseYDir = getVerticalDirection();
			
			if(m_currentTile)
			{
				if(m_mouseYDir == DIR_UP || m_mouseYDir == DIR_DOWN)
				{
					var prevTileHeight:int = m_currentTile.tileHeight;
					m_currentTile.updateByHeight(m_currentMouseY - m_prevMouseY);
					checkNearBordersForTile(m_currentTile, prevTileHeight);
				}
			}
		}
		
		private function onEnterFrameEvent(e:Event):void
		{
			m_prevMouseX = m_currentMouseX;
			m_prevMouseY = m_currentMouseY;
			m_currentMouseX = this.stage.mouseX;
			m_currentMouseY = this.stage.mouseY;
			//m_mouseXDir = getHorizontalDirection();
    		m_mouseYDir = getVerticalDirection();
			
			if(m_currentTile)
			{
				if(m_mouseYDir == DIR_UP)
				{
					//trace("increase tile's height");
				}
				else if(m_mouseYDir == DIR_DOWN)
				{
					//trace("decrease tile's height");
				}
			}
		}
		
		private function getHorizontalDirection():int
		{
			//Compares both positions to determine the direction
			if (m_prevMouseX > m_currentMouseX)
			{
				return DIR_LEFT;
			}
			else if (m_prevMouseX < m_currentMouseX)
			{
				return DIR_RIGHT;
			}
			else
			{
				return DIR_NONE;
			}
		}
		
		private function getVerticalDirection():int
		{
			//Compares both positions to determine the direction
			if (m_prevMouseY > m_currentMouseY)
			{
				return DIR_UP;
			}
			else if (m_prevMouseY < m_currentMouseY)
			{
				return DIR_DOWN;
			}
			else
			{
				return DIR_NONE;
			}
		}
		
		private function onMouseDownEvent(e:MouseEvent):void
		{
			if(e.target.parent is Tile)
			{
				m_currentTile = e.target.parent as Tile;
			}
		}
		
		private function onMouseUpEvent(e:MouseEvent):void
		{
			//trace("onMouseUpEvent");
			//m_isMouseDown = false;
			m_currentTile = null;
		}
	}
}
